###############################################
#           Docker images builds              #
###############################################

#
# Reuse some common commands
#
.docker_prepare:
  extends:
    - .retry
  image: docker:20.10.8
  tags:
    - gitlab-org-docker
  services:
    - docker:20.10.8-dind
  before_script:
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  variables:
    DOCKER_TLS_CERTDIR: "/certs"

#
# Build and deploy the GitLab Docs linting (Markdown) Docker image
#
image:docs-lint-markdown:
  extends:
    - .rules_scheduled_manual
    - .docker_prepare
  stage: build-images
  variables:
    IMAGE_NAME: $CI_REGISTRY_IMAGE/lint-markdown:alpine-$ALPINE_VERSION-vale-$VALE_VERSION-markdownlint-$MARKDOWNLINT_VERSION-markdownlint2-$MARKDOWNLINT2_VERSION
    DOCKERFILE: dockerfiles/gitlab-docs-lint-markdown.Dockerfile
  script:
    - docker build
             --build-arg ALPINE_VERSION=${ALPINE_VERSION}
             --build-arg VALE_VERSION=${VALE_VERSION}
             --build-arg MARKDOWNLINT_VERSION=${MARKDOWNLINT_VERSION}
             --build-arg MARKDOWNLINT2_VERSION=${MARKDOWNLINT2_VERSION}
             --tag $IMAGE_NAME
             --file $DOCKERFILE .
    - docker push $IMAGE_NAME
  environment:
    name: registry/docs-lint-markdown

#
# Test the GitLab Docs linting (Markdown) Docker image if changes are made to its Dockerfile
#
test:image:docs-lint-markdown:
  extends:
    - .docker_prepare
  stage: test
  needs: []
  variables:
    DOCKERFILE: dockerfiles/gitlab-docs-lint-markdown.Dockerfile
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - $DOCKERFILE
  script:
    - docker build
             --build-arg ALPINE_VERSION=${ALPINE_VERSION}
             --build-arg VALE_VERSION=${VALE_VERSION}
             --build-arg MARKDOWNLINT_VERSION=${MARKDOWNLINT_VERSION}
             --file $DOCKERFILE .

#
# Build and deploy the GitLab Docs linting (HTML) Docker image
#
image:docs-lint-html:
  extends:
    - .rules_scheduled_manual
    - .docker_prepare
  stage: build-images
  variables:
    IMAGE_NAME: $CI_REGISTRY_IMAGE/lint-html:alpine-$ALPINE_VERSION-ruby-$RUBY_VERSION-$CI_COMMIT_SHORT_SHA
    DOCKERFILE: dockerfiles/gitlab-docs-lint-html.Dockerfile
  script:
    - docker build
             --build-arg RUBY_VERSION=${RUBY_VERSION}
             --build-arg ALPINE_VERSION=${ALPINE_VERSION}
             --tag $IMAGE_NAME
             --file $DOCKERFILE .
    - docker push $IMAGE_NAME
  environment:
    name: registry/docs-lint-html

#
# Test the GitLab Docs linting (HTML) Docker image if changes are made to its Dockerfile
#
test:image:docs-lint-html:
  extends:
    - .docker_prepare
  stage: test
  needs: []
  variables:
    DOCKERFILE: dockerfiles/gitlab-docs-lint-html.Dockerfile
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - $DOCKERFILE
  script:
    - docker build
             --build-arg RUBY_VERSION=${RUBY_VERSION}
             --build-arg ALPINE_VERSION=${ALPINE_VERSION}
             --file $DOCKERFILE .

#
# Final Docker image containing a single version
# It is based on single.Dockerfile for each branch
#
image:docs-single:
  extends:
    - .docker_prepare
  stage: pre-deploy
  artifacts: {}
  cache: {}
  dependencies: []
  variables:
    IMAGE_NAME: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
    DOCKERFILE: $CI_COMMIT_REF_NAME.Dockerfile
    NANOC_ENV: production
  environment:
    name: registry/$CI_COMMIT_REF_SLUG
  script:
    - docker build --build-arg NANOC_ENV=${NANOC_ENV} --build-arg CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME} -t $IMAGE_NAME -f $DOCKERFILE .
    - docker push $IMAGE_NAME
  # Only branches with versions like 10.4
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^\d{1,2}\.\d{1,2}$/'

#
# Test the GitLab docs single version Docker image if changes made to its Dockerfile
#
test:image:docs-single:
  extends:
    - .docker_prepare
  stage: test
  variables:
    GITLAB_VERSION: '15.5'
    DOCKERFILE: dockerfiles/single.Dockerfile
    IMAGE_NAME: $CI_REGISTRY_IMAGE:$GITLAB_VERSION
    NANOC_ENV: test
  needs: []
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - $DOCKERFILE
  script:
    - apk add --no-cache -U git
    - git fetch -all
    - git checkout -b $GITLAB_VERSION origin/$GITLAB_VERSION
    - docker build
             --build-arg NANOC_ENV=${NANOC_ENV}
             --build-arg VER=${GITLAB_VERSION}
             --build-arg ALGOLIA_SEARCH="true"
             --tag $IMAGE_NAME
             --file $DOCKERFILE .
#
# Final Docker image containing a single version with lunr.js enabled
# It is based on single.Dockerfile for each branch
#
image:docs-single-lunrjs:
  extends:
    - .docker_prepare
  stage: pre-deploy
  artifacts: {}
  cache: {}
  dependencies: []
  variables:
    IMAGE_NAME: $CI_REGISTRY_IMAGE/archives:$CI_COMMIT_REF_NAME
    DOCKERFILE: $CI_COMMIT_REF_NAME.Dockerfile
    NANOC_ENV: production
  environment:
    name: registry-archives/$CI_COMMIT_REF_SLUG
  script:
    - docker build
             --build-arg NANOC_ENV=${NANOC_ENV}
             --build-arg VER=${CI_COMMIT_REF_NAME}
             --build-arg ALGOLIA_SEARCH="false"
             --tag $IMAGE_NAME
             --file $DOCKERFILE .
    - docker push $IMAGE_NAME
  # Only branches with versions like 10.4
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^\d{1,2}\.\d{1,2}$/'

#
# Test the GitLab docs single version Docker image with lunr.js enabled
# if changes made to its Dockerfile
#
test:image:docs-single-lunrjs:
  extends:
    - .docker_prepare
  stage: test
  variables:
    GITLAB_VERSION: '15.5'
    IMAGE_NAME: $CI_REGISTRY_IMAGE/archives:$GITLAB_VERSION
    DOCKERFILE: dockerfiles/single.Dockerfile
    NANOC_ENV: test
  needs: []
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - $DOCKERFILE
  script:
    - apk add --no-cache -U make bash git
    - git fetch -all
    - git checkout -b $GITLAB_VERSION origin/$GITLAB_VERSION
    - docker build
             --build-arg NANOC_ENV=${NANOC_ENV}
             --build-arg VER=${GITLAB_VERSION}
             --build-arg ALGOLIA_SEARCH="false"
             --tag $IMAGE_NAME
             --file $DOCKERFILE .
    - make check-lunr-index

#
# Build master containing the online archives and latest docs (on schedules)
#
image:docs-latest:
  extends:
    - .rules_scheduled_manual
    - .docker_prepare
  stage: build-images
  artifacts: {}
  cache: {}
  dependencies: []
  variables:
    NANOC_ENV: 'production'
    IMAGE_NAME: $CI_REGISTRY_IMAGE:latest
    DOCKERFILE: latest.Dockerfile
  environment:
    name: registry/latest
  script:
    - docker build --build-arg NANOC_ENV=${NANOC_ENV} --build-arg CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME} -t $IMAGE_NAME -f $DOCKERFILE .
    - docker push $IMAGE_NAME

#
# Test the GitLab docs latest Docker image if changes made to its Dockerfile
#
test:image:docs-latest:
  extends:
    - .docker_prepare
  stage: test
  variables:
    IMAGE_NAME: $CI_REGISTRY_IMAGE:latest
    DOCKERFILE: latest.Dockerfile
  needs: []
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - $DOCKERFILE
  script:
    - docker build -t $IMAGE_NAME -f $DOCKERFILE .
    - docker run --rm $IMAGE_NAME ls -l /usr/share/nginx/html
